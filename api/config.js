const path = require("path");

const rootPath = __dirname;

module.exports = {
  serverPort: 8000,
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  dbUrl: "mongodb://localhost/exam12_places_vk",
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
