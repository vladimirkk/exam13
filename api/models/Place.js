const mongoose = require("mongoose");

const PlaceSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true
  }
});

const Place = mongoose.model("Place", PlaceSchema);

module.exports = Place;
