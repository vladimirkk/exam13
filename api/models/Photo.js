const mongoose = require("mongoose");

const PhotoSchema = new mongoose.Schema({
  image: {
    type: String,
    required: true
  },
  place: {
    type: mongoose.Schema.ObjectId,
    ref: "Place",
    required: true
  }
});

const Photo = mongoose.model("Photo", PhotoSchema);

module.exports = Photo;
