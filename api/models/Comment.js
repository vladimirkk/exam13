const mongoose = require("mongoose");

const RatingSchema = new mongoose.Schema({
  food: {
    type: Number,
    required: true,
    min: 1,
    max: 5
  },
  service: {
    type: Number,
    required: true,
    min: 1,
    max: 5
  },
  interior: {
    type: Number,
    required: true,
    min: 1,
    max: 5
  }
});

const CommentSchema = new mongoose.Schema({
  comment: {
    type: String,
    required: true
  },
  ratings: {
    type: RatingSchema,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  place: {
    type: mongoose.Schema.ObjectId,
    ref: "Place",
    required: true
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true
  }
});

const Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;
