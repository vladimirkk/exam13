const mongoose = require("mongoose");
const nanoid = require("nanoid");
const config = require("./config");

const User = require("./models/User");
const Photo = require("./models/Photo");
const Place = require("./models/Place");
const Comment = require("./models/Comment");

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [admin, user1, user2, user3] = await User.create(
    {
      username: "admin",
      password: "123",
      displayName: "Test Admin #1",
      avatar: "_avatar3.jpg",
      role: "admin",
      token: nanoid()
    },
    {
      username: "user1",
      password: "123",
      displayName: "Test User #1",
      avatar: "_avatar1.jpg",
      token: nanoid()
    },
    {
      username: "user2",
      password: "123",
      displayName: "Test User #2",
      avatar: "_avatar2.jpg",
      token: nanoid()
    },
    {
      username: "user3",
      password: "123",
      displayName: "Test User #3",
      avatar: "_avatar3.jpg",
      token: nanoid()
    }
  );

  const [place1, place2] = await Place.create(
    {
      title: "Some cafe",
      description: "Some description here",
      image: "_image1.jpg",
      user: user1._id
    },
    {
      title: "Another cafe",
      description: "Description will be here",
      image: "_image6.jpg",
      user: user2._id
    }
  );

  await Photo.create(
    {
      image: "_image1.jpg",
      place: place1._id
    },
    {
      image: "_image2.jpg",
      place: place1._id
    },
    {
      image: "_image3.jpg",
      place: place1._id
    },
    {
      image: "_image4.jpg",
      place: place1._id
    },
    {
      image: "_image5.jpg",
      place: place1._id
    },
    {
      image: "_image6.jpg",
      place: place2._id
    },
    {
      image: "_image7.jpg",
      place: place2._id
    },
    {
      image: "_image8.jpg",
      place: place2._id
    },
    {
      image: "_image9.jpg",
      place: place2._id
    },
    {
      image: "_image10.jpg",
      place: place2._id
    }
  );

  await Comment.create(
    {
      comment: "This is comment",
      ratings: {
        food: 5,
        service: 4,
        interior: 5
      },
      place: place2._id,
      user: user1._id,
      date: new Date()
    },
    {
      comment: "Another comment",
      ratings: {
        food: 5,
        service: 5,
        interior: 3
      },
      place: place1._id,
      user: user2._id,
      date: new Date()
    },
    {
      comment: "This is comment",
      ratings: {
        food: 2,
        service: 5,
        interior: 5
      },
      place: place2._id,
      user: user3._id,
      date: new Date()
    },
    {
      comment: "This is comment",
      ratings: {
        food: 4,
        service: 4,
        interior: 4
      },
      place: place1._id,
      user: user3._id,
      date: new Date()
    },
    {
      comment: "Admin comment",
      ratings: {
        food: 4,
        service: 5,
        interior: 3
      },
      place: place2._id,
      user: admin._id,
      date: new Date()
    }
  );

  return connection.close();
};

run().catch(error => {
  console.error("Something wrong happened...", error);
});
