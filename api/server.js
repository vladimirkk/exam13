const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config");
const users = require("./app/users");
const places = require("./app/places");
const comments = require("./app/comments");
const photos = require("./app/photos");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use("/users", users);
  app.use("/places", places);
  app.use("/comments", comments);
  app.use("/photos", photos);

  app.listen(config.serverPort, () => {
    console.log(`[Server] started on ${config.serverPort} port`);
  });
});
