const express = require("express");
const auth = require("../middleware/auth");
const Comment = require("../models/Comment");

const router = express.Router();

router.post("/", auth(), async (req, res) => {
  try {
    const userComments = await Comment.find({ user: req.user._id, place: req.body.place });

    if (userComments.length > 0) {
      return res.status(400).send({ message: "This user already has a comment on this place!" });
    }

    const commentData = {
      comment: req.body.comment,
      ratings: req.body.ratings,
      place: req.body.place,
      date: new Date(),
      user: req.user._id
    };

    const result = await new Comment(commentData).save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete("/:id", auth(), async (req, res) => {
  try {
    const comment = await Comment.findById(req.params.id);

    if (!comment) {
      return res.sendStatus(404);
    }

    if (!comment.user.equals(req.user._id) && req.user.role !== "admin") {
      return res.sendStatus(403);
    }

    await Comment.deleteOne({ _id: comment._id });

    res.send({ message: "Successfully deleted" });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
