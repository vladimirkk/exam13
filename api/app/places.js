const express = require("express");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const upload = require("../middleware/upload");
const Place = require("../models/Place");
const Comment = require("../models/Comment");
const Photo = require("../models/Photo");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const places = await Place.find();

    const placesPromises = places.map(async place => {
      const comments = await Comment.find({ place: place._id });

      const ratings = comments.reduce((num, comment) => {
        const { food, service, interior } = comment.ratings;
        const avgRating = (food + service + interior) / 3;

        num.push(avgRating);

        return num;
      }, []);

      const sumOfRatings = ratings.reduce((previous, current) => (current += previous), 0);
      const avgOfRatings = sumOfRatings / ratings.length || 0;

      const photos = await Photo.find({ place: place._id });

      return {
        _id: place._id,
        title: place.title,
        image: place.image,
        rating: avgOfRatings,
        comments: comments.length,
        photos: photos.length
      };
    });

    const result = await Promise.all(placesPromises);

    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const { id } = req.params;

    const place = await Place.findById(id).populate("user", ["displayName", "avatar"]);
    res.send(place);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/:id/photos", async (req, res) => {
  try {
    const { id } = req.params;

    const photos = await Photo.find({ place: id });
    res.send(photos);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/:id/comments", async (req, res) => {
  try {
    const { id } = req.params;

    const comments = await Comment.find({ place: id }).populate("user", ["displayName", "avatar"]);
    res.send(comments);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post("/", [auth(), upload.single("image")], (req, res) => {
  if (!req.body.confirmation || JSON.parse(req.body.confirmation) === false) {
    return res.sendStatus(403);
  }

  const placeData = {
    title: req.body.title,
    description: req.body.description,
    user: req.user._id
  };

  if (req.file) {
    placeData.image = req.file.filename;
  }

  const place = new Place(placeData);

  place
    .save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete("/:id", [auth(), permit("admin")], async (req, res) => {
  try {
    const place = await Place.findById(req.params.id);

    if (!place) {
      return res.sendStatus(404);
    }

    await Place.deleteOne({ _id: place._id });

    res.send({ message: "Successfully deleted" });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
