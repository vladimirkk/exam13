const express = require("express");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const upload = require("../middleware/upload");
const Photo = require("../models/Photo");
const Place = require("../models/Place");

const router = express.Router();

router.post("/", [auth(), upload.array("images")], async (req, res) => {
  try {
    const photoData = {
      place: req.body.place
    };

    const place = await Place.findById(photoData.place);

    if (!place) {
      return res.sendStatus(404);
    }

    if (req.files) {
      const files = req.files.map(file => ({ ...photoData, image: file.filename }));

      const result = await Photo.create(files);

      return res.send(result);
    } else {
      return res.sendStatus(400);
    }
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete("/:id", [auth(), permit("admin")], async (req, res) => {
  try {
    const photo = await Photo.findById(req.params.id);

    if (!photo) {
      return res.sendStatus(404);
    }

    await photo.deleteOne({ _id: photo._id });

    res.send({ message: "Successfully deleted" });
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
