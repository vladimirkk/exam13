import React, { Component, Fragment } from "react";
import { NotificationContainer } from "react-notifications";
import { connect } from "react-redux";
import * as actions from "./store/actions";
import Routes from "./Routes";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-notifications/lib/notifications.css";
import Layout from "./components/Layout/Layout";

class App extends Component {
  componentDidMount() {
    if (this.props.user) {
      this.props.checkAuth();
    }
  }

  render() {
    return (
      <Fragment>
        <NotificationContainer />
        <Layout>
          <Routes user={this.props.user} />
        </Layout>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  checkAuth: () => dispatch(actions.checkAuth())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
