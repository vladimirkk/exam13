import axios from "../../axios-api";
import { NotificationManager } from "react-notifications";
import * as actionTypes from "../actionTypes";

const fetchPlacePhotosRequest = () => ({ type: actionTypes.FETCH_PLACE_PHOTOS_REQUEST });

const fetchPlacePhotosSuccess = photos => ({
  type: actionTypes.FETCH_PLACE_PHOTOS_SUCCESS,
  photos
});

const fetchPlacePhotosFailure = error => ({
  type: actionTypes.FETCH_PLACE_PHOTOS_FAILURE,
  error
});

export const fetchPlacePhotos = placeId => {
  return dispatch => {
    dispatch(fetchPlacePhotosRequest());
    return axios.get(`/places/${placeId}/photos`).then(
      response => dispatch(fetchPlacePhotosSuccess(response.data)),
      error => {
        if (error.response) {
          dispatch(fetchPlacePhotosFailure(error.response.data));
          NotificationManager.error("An error occurred while loading place photos!");
        } else {
          dispatch(fetchPlacePhotosFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const uploadPlacePhotoRequest = () => ({ type: actionTypes.UPLOAD_PLACE_PHOTO_REQUEST });
const uploadPlacePhotoSuccess = () => ({ type: actionTypes.UPLOAD_PLACE_PHOTO_SUCCESS });
const uploadPlacePhotoFailure = () => ({ type: actionTypes.UPLOAD_PLACE_PHOTO_FAILURE });

export const uploadPlacePhoto = photoData => {
  return dispatch => {
    dispatch(uploadPlacePhotoRequest());
    return axios.post("/photos", photoData).then(
      () => {
        dispatch(uploadPlacePhotoSuccess());
        NotificationManager.success("Photo successfully uploaded!");
        dispatch(fetchPlacePhotos(photoData.get("place")));
      },
      error => {
        if (error.response) {
          dispatch(uploadPlacePhotoFailure(error.response.data));
          NotificationManager.error("An error occurred while uploading place photo!");
        } else {
          dispatch(uploadPlacePhotoFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const deletePlacePhotoRequest = () => ({ type: actionTypes.DELETE_PLACE_PHOTO_REQUEST });
const deletePlacePhotoSuccess = () => ({ type: actionTypes.DELETE_PLACE_PHOTO_SUCCESS });
const deletePlacePhotoFailure = () => ({ type: actionTypes.DELETE_PLACE_PHOTO_FAILURE });

export const deletePlacePhoto = (photoId, placeId) => {
  return dispatch => {
    dispatch(deletePlacePhotoRequest());
    return axios.delete(`/photos/${photoId}`).then(
      () => {
        dispatch(deletePlacePhotoSuccess());
        NotificationManager.success("Photo successfully deleted!");
        dispatch(fetchPlacePhotos(placeId));
      },
      error => {
        if (error.response) {
          dispatch(deletePlacePhotoFailure(error.response.data));
          NotificationManager.error("An error occurred while deleting place photo!");
        } else {
          dispatch(deletePlacePhotoFailure({ global: "No connection" }));
        }
      }
    );
  };
};
