import axios from "../../axios-api";
import { NotificationManager } from "react-notifications";
import * as actionTypes from "../actionTypes";
import { push } from "connected-react-router";

const fetchPlacesRequest = () => ({ type: actionTypes.FETCH_PLACES_REQUEST });
const fetchPlacesSuccess = places => ({ type: actionTypes.FETCH_PLACES_SUCCESS, places });
const fetchPlacesFailure = error => ({ type: actionTypes.FETCH_PLACES_FAILURE, error });

export const fetchPlaces = () => {
  return dispatch => {
    dispatch(fetchPlacesRequest());
    return axios.get("/places").then(
      response => dispatch(fetchPlacesSuccess(response.data)),
      error => {
        if (error.response) {
          dispatch(fetchPlacesFailure(error.response.data));
          NotificationManager.error("An error occurred while loading places!");
        } else {
          dispatch(fetchPlacesFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const fetchPlaceInfoRequest = () => ({ type: actionTypes.FETCH_PLACE_INFO_REQUEST });
const fetchPlaceInfoSuccess = place => ({ type: actionTypes.FETCH_PLACE_INFO_SUCCESS, place });
const fetchPlaceInfoFailure = error => ({ type: actionTypes.FETCH_PLACE_INFO_FAILURE, error });

export const fetchPlaceInfo = id => {
  return dispatch => {
    dispatch(fetchPlaceInfoRequest());
    return axios.get(`/places/${id}`).then(
      response => dispatch(fetchPlaceInfoSuccess(response.data)),
      error => {
        if (error.response) {
          dispatch(fetchPlaceInfoFailure(error.response.data));
          NotificationManager.error("An error occurred while loading place!");
        } else {
          dispatch(fetchPlaceInfoFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const addPlaceRequest = () => ({ type: actionTypes.ADD_PLACE_REQUEST });
const addPlaceSuccess = () => ({ type: actionTypes.ADD_PLACE_SUCCESS });
const addPlaceFailure = error => ({ type: actionTypes.ADD_PLACE_FAILURE, error });

export const addPlace = placeData => {
  return dispatch => {
    dispatch(addPlaceRequest());
    return axios.post("/places", placeData).then(
      response => {
        dispatch(addPlaceSuccess(response.data));
        NotificationManager.success("Place added successfully!");
        dispatch(push("/"));
      },
      error => {
        if (error.response) {
          dispatch(addPlaceFailure(error.response.data));
          NotificationManager.error("An error occurred while adding place!");
        } else {
          dispatch(addPlaceFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const deletePlaceRequest = () => ({ type: actionTypes.DELETE_PLACE_REQUEST });
const deletePlaceSuccess = () => ({ type: actionTypes.DELETE_PLACE_SUCCESS });
const deletePlaceFailure = error => ({ type: actionTypes.DELETE_PLACE_FAILURE, error });

export const deletePlace = placeId => {
  return dispatch => {
    dispatch(deletePlaceRequest());
    return axios.delete(`/places/${placeId}`).then(
      response => {
        dispatch(deletePlaceSuccess(response.data));
        NotificationManager.success("Cafe deleted successfully!");
        dispatch(fetchPlaces());
      },
      error => {
        if (error.response) {
          dispatch(deletePlaceFailure(error.response.data));
          NotificationManager.error("An error occurred while deleting cafe!");
        } else {
          dispatch(deletePlaceFailure({ global: "No connection" }));
        }
      }
    );
  };
};
