import axios from "../../axios-api";
import { NotificationManager } from "react-notifications";
import * as actionTypes from "../actionTypes";

const fetchPlaceCommentsRequest = () => ({ type: actionTypes.FETCH_PLACE_COMMENTS_REQUEST });

const fetchPlaceCommentsSuccess = comments => ({
  type: actionTypes.FETCH_PLACE_COMMENTS_SUCCESS,
  comments
});

const fetchPlaceCommentsFailure = error => ({
  type: actionTypes.FETCH_PLACE_COMMENTS_FAILURE,
  error
});

export const fetchPlaceComments = placeId => {
  return dispatch => {
    dispatch(fetchPlaceCommentsRequest());
    return axios.get(`/places/${placeId}/comments`).then(
      response => dispatch(fetchPlaceCommentsSuccess(response.data)),
      error => {
        if (error.response) {
          dispatch(fetchPlaceCommentsFailure(error.response.data));
          NotificationManager.error("An error occurred while loading place comments!");
        } else {
          dispatch(fetchPlaceCommentsFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const createPlaceCommentRequest = () => ({ type: actionTypes.CREATE_PLACE_COMMENT_REQUEST });
const createPlaceCommentSuccess = () => ({ type: actionTypes.CREATE_PLACE_COMMENT_SUCCESS });
const createPlaceCommentFailure = () => ({ type: actionTypes.CREATE_PLACE_COMMENT_FAILURE });

export const createPlaceComment = (placeId, comment, ratings) => {
  return dispatch => {
    dispatch(createPlaceCommentRequest());
    return axios.post("/comments", { place: placeId, comment, ratings }).then(
      () => {
        dispatch(createPlaceCommentSuccess());
        NotificationManager.success("Comment successfully added.");
        dispatch(fetchPlaceComments(placeId));
      },
      error => {
        if (error.response) {
          dispatch(createPlaceCommentFailure(error.response.data));
          NotificationManager.error("An error occurred while creating place comment!");
        } else {
          dispatch(createPlaceCommentFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const deletePlaceCommentRequest = () => ({ type: actionTypes.DELETE_PLACE_COMMENT_REQUEST });
const deletePlaceCommentSuccess = () => ({ type: actionTypes.DELETE_PLACE_COMMENT_SUCCESS });
const deletePlaceCommentFailure = () => ({ type: actionTypes.DELETE_PLACE_COMMENT_FAILURE });

export const deletePlaceComment = (commentId, placeId) => {
  return dispatch => {
    dispatch(deletePlaceCommentRequest());
    return axios.delete(`/comments/${commentId}`).then(
      () => {
        dispatch(deletePlaceCommentSuccess());
        NotificationManager.success("Comment successfully deleted.");
        dispatch(fetchPlaceComments(placeId));
      },
      error => {
        if (error.response) {
          dispatch(deletePlaceCommentFailure(error.response.data));
          NotificationManager.error("An error occurred while deleting place comment!");
        } else {
          dispatch(deletePlaceCommentFailure({ global: "No connection" }));
        }
      }
    );
  };
};
