import axios from "../../axios-api";
import { push } from "connected-react-router";
import { NotificationManager } from "react-notifications";
import * as actionTypes from "../actionTypes";

const registerUserSuccess = user => ({ type: actionTypes.REGISTER_USER_SUCCESS, user });
const registerUserFailure = error => ({ type: actionTypes.REGISTER_USER_FAILURE, error });

export const registerUser = userData => {
  return dispatch => {
    return axios.post("/users", userData).then(
      response => {
        dispatch(registerUserSuccess(response.data));
        NotificationManager.success("Registered successfully!");
        dispatch(push("/"));
      },
      error => {
        if (error.response) {
          dispatch(registerUserFailure(error.response.data));
        } else {
          dispatch(registerUserFailure({ global: "No connection" }));
        }
      }
    );
  };
};

const loginUserSuccess = user => ({ type: actionTypes.LOGIN_USER_SUCCESS, user });

export const loginUser = userData => {
  return dispatch => {
    return axios.post("/users/sessions", userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        NotificationManager.success("Logged in successfully!");
        dispatch(push("/"));
      },
      error => {
        if (error.response) {
          NotificationManager.error(error.response.data.error, "Authentication error!", 5000);
        } else {
          NotificationManager.error("No connection", "Authentication error!", 5000);
        }
      }
    );
  };
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = { headers: { Authorization: token } };

    return axios.delete("/users/sessions", config).then(
      () => {
        dispatch({ type: actionTypes.LOGOUT_USER });
        NotificationManager.success("Logged out!");
      },
      () => {
        NotificationManager.error("Could not logout");
      }
    );
  };
};

export const checkAuth = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    axios
      .get("/users/sessions", { headers: { Authorization: token } })
      .then(null, () => dispatch({ type: actionTypes.LOGOUT_USER }));
  };
};
