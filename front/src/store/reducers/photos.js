import * as actionTypes from "../actionTypes";

const initialState = {
  photos: [],
  loading: true
};

const photos = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PLACE_PHOTOS_REQUEST:
      return { ...state, loading: true };
    case actionTypes.FETCH_PLACE_PHOTOS_SUCCESS:
      return { ...state, loading: false, photos: action.photos };
    case actionTypes.FETCH_PLACE_PHOTOS_FAILURE:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default photos;
