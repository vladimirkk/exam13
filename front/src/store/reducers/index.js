import users from "./users";
import places from "./places";
import comments from "./comments";
import photos from "./photos";

export default {
  users,
  places,
  comments,
  photos
};
