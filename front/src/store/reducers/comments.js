import * as actionTypes from "../actionTypes";

const initialState = {
  comments: [],
  loading: true
};

const comments = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PLACE_COMMENTS_REQUEST:
      return { ...state, loading: true };
    case actionTypes.FETCH_PLACE_COMMENTS_SUCCESS:
      return { ...state, loading: false, comments: action.comments };
    case actionTypes.FETCH_PLACE_COMMENTS_FAILURE:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default comments;
