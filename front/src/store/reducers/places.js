import * as actionTypes from "../actionTypes";

const initialState = {
  places: [],
  placeInfo: null,
  loading: true
};

const places = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PLACES_REQUEST:
      return { ...state, loading: true };
    case actionTypes.FETCH_PLACES_SUCCESS:
      return { ...state, loading: false, places: action.places };
    case actionTypes.FETCH_PLACES_FAILURE:
      return { ...state, loading: false };
    case actionTypes.FETCH_PLACE_INFO_REQUEST:
      return { ...state, loading: true };
    case actionTypes.FETCH_PLACE_INFO_SUCCESS:
      return { ...state, loading: false, placeInfo: action.place };
    case actionTypes.FETCH_PLACE_INFO_FAILURE:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default places;
