import React, { Component } from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import "./Register.css";
import { connect } from "react-redux";
import * as actions from "../../store/actions";

class Register extends Component {
  state = {
    displayName: "",
    username: "",
    password: "",
    avatar: null
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onChangeAvatar = event => {
    this.setState({
      avatar: event.target.files[0]
    });
  };

  onFormSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.registerUser(formData);
  };

  render() {
    return (
      <Form onSubmit={this.onFormSubmit} className="Register">
        <h2>Create Account</h2>
        <FormGroup>
          <Input
            type="text"
            name="displayName"
            placeholder="Nickname"
            value={this.state.displayName}
            onChange={this.onChange}
          />
        </FormGroup>
        <FormGroup>
          <Input
            type="text"
            name="username"
            placeholder="Login"
            value={this.state.username}
            onChange={this.onChange}
          />
        </FormGroup>
        <FormGroup>
          <Input type="password" name="password" placeholder="Password" onChange={this.onChange} />
        </FormGroup>
        <FormGroup>
          <Label for="avatar">Avatar</Label>
          <Input type="file" name="avatar" id="avatar" onChange={this.onChangeAvatar} />
        </FormGroup>
        <Button size="lg">Register</Button>
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(actions.registerUser(userData))
});

export default connect(
  null,
  mapDispatchToProps
)(Register);
