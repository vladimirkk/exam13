import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import { NavLink, withRouter } from "react-router-dom";
import * as actions from "../../store/actions";
import { apiURL } from "../../constants";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink as BootstrapNavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container
} from "reactstrap";
import "./Header.css";

const Authorization = () => {
  return (
    <Fragment>
      <NavItem>
        <BootstrapNavLink tag={NavLink} to="/register">
          Sign up
        </BootstrapNavLink>
      </NavItem>
      <NavItem>
        <BootstrapNavLink tag={NavLink} to="/login">
          Log in
        </BootstrapNavLink>
      </NavItem>
    </Fragment>
  );
};

const UserMenu = ({ user, logoutUser, history }) => {
  const goToAddCafePage = () => history.push("/places/add");

  return (
    <UncontrolledDropdown nav inNavbar className="HeaderUserDropdown">
      <DropdownToggle nav caret>
        <img src={`${apiURL}/uploads/${user.avatar}`} alt="avatar" height={40} width={40} />
        {user.displayName}
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem onClick={goToAddCafePage}>Add new place</DropdownItem>
        <DropdownItem divider />
        <DropdownItem onClick={logoutUser}>Logout</DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

const Header = ({ user, logoutUser, history }) => {
  const [open, setOpen] = useState(false);

  const toggleDropDown = () => setOpen(!open);

  return (
    <header className="Header">
      <Navbar color="dark" dark expand="md">
        <Container>
          <NavbarBrand tag={NavLink} to="/">
            Cafe Critic
          </NavbarBrand>
          <NavbarToggler onClick={toggleDropDown} />
          <Collapse isOpen={open} navbar>
            <Nav className="ml-auto" navbar>
              {user ? (
                <UserMenu user={user} logoutUser={logoutUser} history={history} />
              ) : (
                <Authorization />
              )}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(actions.logoutUser())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Header));
