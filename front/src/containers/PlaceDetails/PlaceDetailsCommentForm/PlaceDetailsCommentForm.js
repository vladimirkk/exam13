import React, { Fragment, useState } from "react";
import { Button, Form, FormGroup, Input } from "reactstrap";
import Rating from "../../../components/UI/Rating/Rating";
import "./PlaceDetailsCommentForm.css";

const PlaceDetailsCommentForm = ({ placeId, createPlaceComment }) => {
  const [comment, setComment] = useState("");
  const [food, setFood] = useState(5);
  const [service, setService] = useState(5);
  const [interior, setInterior] = useState(5);

  const onCommentChange = event => setComment(event.target.value);

  const onSubmit = event => {
    event.preventDefault();

    createPlaceComment(placeId, comment, { food, service, interior });
  };

  return (
    <Fragment>
      <h3>Add review</h3>
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <Input
            type="textarea"
            id="reviewText"
            rows={5}
            value={comment}
            onChange={onCommentChange}
          />
        </FormGroup>
        <FormGroup className="PlaceDetailsCommentForm-ratings">
          <div>
            <span>Quality of food:</span>
            <Rating
              id="qualityOfFood"
              value={food}
              onChange={setFood}
              readonly={false}
              fractions={1}
            />
          </div>
          <div>
            <span>Service quality:</span>
            <Rating
              id="serviceQuality"
              value={service}
              onClick={setService}
              readonly={false}
              fractions={1}
            />
          </div>
          <div>
            <span>Interior:</span>
            <Rating
              id="interior"
              value={interior}
              onClick={setInterior}
              readonly={false}
              fractions={1}
            />
          </div>
          <Button>Add</Button>
        </FormGroup>
      </Form>
    </Fragment>
  );
};

export default PlaceDetailsCommentForm;
