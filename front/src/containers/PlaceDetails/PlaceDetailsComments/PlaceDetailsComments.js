import React, { Fragment, useState } from "react";
import moment from "moment";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardText,
  Button,
  Modal,
  ModalBody,
  ModalFooter
} from "reactstrap";
import Rating from "../../../components/UI/Rating/Rating";
import Preloader from "../../../components/UI/Preloader/Preloader";
import deleteIcon from "../../../assets/images/delete.svg";
import "./PlaceDetailsComments.css";

const PlaceComment = ({
  id,
  date,
  author,
  comment,
  ratings,
  showDeleteBtn,
  placeId,
  deletePlaceComment
}) => {
  const [confirm, setConfirm] = useState(false);
  const toggleConfirm = () => setConfirm(prevState => !prevState);

  const deleteHandler = () => {
    deletePlaceComment(id, placeId);
    toggleConfirm();
  };

  return (
    <Fragment>
      <Card>
        <CardHeader className="PlaceDetailsComments-header">
          <span>
            On {moment(date).format("DD.MM.YYYY hh:mm")} <b>{author}</b> wrote:
          </span>
          {showDeleteBtn && (
            <Button onClick={toggleConfirm}>
              <img src={deleteIcon} alt="Delete comment" />
            </Button>
          )}
        </CardHeader>
        <CardBody>
          <CardText>{comment}</CardText>
        </CardBody>
        <CardFooter className="PlaceDetailsComments-ratings">
          <div>
            <span>Quality of food:</span>
            <Rating value={ratings.food} />
            <span>({ratings.food.toFixed(1)})</span>
          </div>
          <div>
            <span>Service quality:</span>
            <Rating value={ratings.service} />
            <span>({ratings.service.toFixed(1)})</span>
          </div>
          <div>
            <span>Interior:</span>
            <Rating value={ratings.interior} />
            <span>({ratings.interior.toFixed(1)})</span>
          </div>
        </CardFooter>
      </Card>
      {confirm && (
        <Modal isOpen={confirm} toggle={toggleConfirm}>
          <ModalBody>Are you sure you want to delete comment?</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={deleteHandler} size="sm">
              Continue
            </Button>
            <Button color="secondary" onClick={toggleConfirm} size="sm">
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </Fragment>
  );
};

const PlaceDetailsComments = ({ user, list, deletePlaceComment, loading }) => {
  let comments = [...list];

  if (user) {
    const userComment = list.filter(comment => comment.user._id === user._id);
    const otherComments = list.filter(comment => comment.user._id !== user._id);

    comments = [...userComment, ...otherComments];
  }

  if (loading) return <Preloader />;

  return (
    <div className="PlaceDetailsComments">
      <h3>Reviews</h3>
      {comments.map(comment => {
        const userIsOwner = user && user._id === comment.user._id;
        const userIsAdmin = user && user.role === "admin";

        return (
          <PlaceComment
            key={comment._id}
            id={comment._id}
            author={comment.user.displayName}
            date={comment.date}
            comment={comment.comment}
            ratings={comment.ratings}
            showDeleteBtn={userIsOwner || userIsAdmin}
            placeId={comment.place}
            deletePlaceComment={deletePlaceComment}
          />
        );
      })}
    </div>
  );
};

export default PlaceDetailsComments;
