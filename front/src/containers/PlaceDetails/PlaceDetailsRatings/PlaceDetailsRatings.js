import React, { Fragment } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
import Rating from "../../../components/UI/Rating/Rating";
import Preloader from "../../../components/UI/Preloader/Preloader";
import "./PlaceDetailsRatings.css";

const PlaceDetailsRatings = ({ food, service, interior, loading }) => {
  const overall = (food + service + interior) / 3;

  if (loading) return <Preloader />;

  return (
    <Fragment>
      <h3>Average ratings</h3>
      <ListGroup className="PlaceDetailsRatings">
        <ListGroupItem>
          <span>Overall:</span>
          <Rating value={overall} />
          <span>{overall.toFixed(1)}</span>
        </ListGroupItem>
        <ListGroupItem>
          <span>Quality of food:</span>
          <Rating value={food} />
          <span>{food.toFixed(1)}</span>
        </ListGroupItem>
        <ListGroupItem>
          <span>Service quality:</span>
          <Rating value={service} />
          <span>{service.toFixed(1)}</span>
        </ListGroupItem>
        <ListGroupItem>
          <span>Interior:</span>
          <Rating value={interior} />
          <span>{interior.toFixed(1)}</span>
        </ListGroupItem>
      </ListGroup>
    </Fragment>
  );
};

export default PlaceDetailsRatings;
