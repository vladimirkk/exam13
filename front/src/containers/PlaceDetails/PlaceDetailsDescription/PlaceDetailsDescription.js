import React from "react";
import { apiURL } from "../../../constants";
import Preloader from "../../../components/UI/Preloader/Preloader";
import "./PlaceDetailsDescription.css";

const PlaceDetailsDescription = ({ data, loading }) => {
  if (loading) return <Preloader />;

  return (
    <div className="PlaceDetailsDescription">
      <img src={`${apiURL}/uploads/${data && data.image}`} alt="" />
      <h2>{data && data.title}</h2>
      <p>{data && data.description}</p>
    </div>
  );
};

export default PlaceDetailsDescription;
