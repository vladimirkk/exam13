import React, { useState } from "react";
import { Button, Form } from "reactstrap";
import "./PlaceDetailsImageForm.css";

const PlaceDetailsImageForm = ({ placeId, uploadPlacePhoto }) => {
  const [files, setFiles] = useState({});

  const onFileChange = event => {
    setFiles(event.target.files);
  };

  const fileNames = Object.values(files).map(file => file.name);

  const labelText = fileNames.length > 0 ? fileNames.join(", ") : "Choose file";

  const onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(files).forEach(key => {
      formData.append("images", files[key]);
    });

    formData.append("place", placeId);

    uploadPlacePhoto(formData);
  };

  return (
    <div className="PlaceDetailsImageForm">
      <h3>Upload new photo</h3>
      <Form onSubmit={onSubmit}>
        <div className="custom-file">
          <input
            type="file"
            className="custom-file-input"
            id="customFile"
            accept="image/*"
            onChange={onFileChange}
            multiple
          />
          <label className="custom-file-label" htmlFor="customFile">
            {labelText}
          </label>
        </div>
        <Button>Upload</Button>
      </Form>
    </div>
  );
};

export default PlaceDetailsImageForm;
