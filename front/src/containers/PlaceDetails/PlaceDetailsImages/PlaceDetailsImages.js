import React, { Fragment, useState } from "react";
import { Button, Col, Modal, ModalBody, ModalFooter, Row } from "reactstrap";
import { apiURL } from "../../../constants";
import Preloader from "../../../components/UI/Preloader/Preloader";
import deleteIcon from "../../../assets/images/delete.svg";
import "./PlaceDetailsImages.css";

const PlaceDetailsImages = ({ placeId, user, urls, deletePlacePhoto, loading }) => {
  const [modalImage, setModalImage] = useState(null);
  const [modalShow, setModalShow] = useState(false);

  const toggleModal = () => setModalShow(state => !state);

  const [confirm, setConfirm] = useState(false);
  const closeConfirm = () => setConfirm(false);
  const openConfirm = imageId => setConfirm(imageId);

  const deletePhoto = () => {
    deletePlacePhoto(confirm, placeId);
    setConfirm(false);
  };

  if (loading) return <Preloader />;

  const images = urls.map(image => {
    const imageUrl = `${apiURL}/uploads/${image.image}`;

    const viewImage = () => {
      setModalImage(imageUrl);
      toggleModal();
    };

    const deleteHandler = event => {
      event.stopPropagation();

      openConfirm(event.currentTarget.id);
    };

    const userIsAdmin = user && user.role === "admin";

    return (
      <Col key={image._id} sm={6} md={3} lg={2} onClick={viewImage}>
        {userIsAdmin && (
          <button type="button" id={image._id} onClick={deleteHandler}>
            <img src={deleteIcon} alt="" />
          </button>
        )}
        <img src={imageUrl} alt="" />
      </Col>
    );
  });

  return (
    <Fragment>
      <h3>Gallery</h3>
      <Row className="PlaceDetailsImages">
        {images.length === 0 ? <Col tag="p">No photo yet...</Col> : images}
      </Row>
      <Modal isOpen={modalShow} toggle={toggleModal} centered size="lg">
        <ModalBody style={{ textAlign: "center" }}>
          <img src={modalImage} alt="" width="100%" />
        </ModalBody>
      </Modal>
      {confirm !== false && (
        <Modal isOpen={true} toggle={closeConfirm}>
          <ModalBody>Are you sure you want to delete photo?</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={deletePhoto} size="sm">
              Continue
            </Button>
            <Button color="secondary" onClick={closeConfirm} size="sm">
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      )}
    </Fragment>
  );
};

export default PlaceDetailsImages;
