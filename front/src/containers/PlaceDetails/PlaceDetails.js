import React, { Component, Fragment } from "react";
import PlaceDetailsDescription from "./PlaceDetailsDescription/PlaceDetailsDescription";
import PlaceDetailsImages from "./PlaceDetailsImages/PlaceDetailsImages";
import PlaceDetailsRatings from "./PlaceDetailsRatings/PlaceDetailsRatings";
import PlaceDetailsComments from "./PlaceDetailsComments/PlaceDetailsComments";
import PlaceDetailsCommentForm from "./PlaceDetailsCommentForm/PlaceDetailsCommentForm";
import PlaceDetailsImageForm from "./PlaceDetailsImageForm/PlaceDetailsImageForm";
import { connect } from "react-redux";
import * as actions from "../../store/actions";

const getAverageRatings = comments => {
  const sumOfRatings = comments.reduce(
    (obj, comment) => ({
      food: obj.food + comment.ratings.food,
      service: obj.service + comment.ratings.service,
      interior: obj.interior + comment.ratings.interior
    }),
    { food: 0, service: 0, interior: 0 }
  );

  return {
    food: sumOfRatings.food / comments.length || 0,
    service: sumOfRatings.service / comments.length || 0,
    interior: sumOfRatings.interior / comments.length || 0
  };
};

class PlaceDetails extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.fetchPlaceInfo(id);
    this.props.fetchPlaceComments(id);
    this.props.fetchPlacePhotos(id);
  }

  render() {
    const {
      user,
      placeLoading,
      place,
      commentsLoading,
      comments,
      photosLoading,
      photos,
      createPlaceComment,
      deletePlaceComment,
      uploadPlacePhoto,
      deletePlacePhoto
    } = this.props;

    const ratings = getAverageRatings(comments);

    const userHasComment = () => {
      return comments.filter(comment => comment.user._id === user._id).length > 0;
    };

    return (
      <Fragment>
        <PlaceDetailsDescription data={place} loading={placeLoading} />
        <hr />
        <PlaceDetailsImages
          placeId={place && place._id}
          user={user}
          urls={photos}
          deletePlacePhoto={deletePlacePhoto}
          loading={photosLoading}
        />
        <hr />
        <PlaceDetailsRatings
          food={ratings.food}
          service={ratings.service}
          interior={ratings.interior}
          loading={commentsLoading}
        />
        <hr />
        <PlaceDetailsComments
          user={user}
          list={comments}
          deletePlaceComment={deletePlaceComment}
          loading={commentsLoading}
        />
        {user && !userHasComment() && (
          <Fragment>
            <hr />
            <PlaceDetailsCommentForm
              placeId={place && place._id}
              createPlaceComment={createPlaceComment}
            />
          </Fragment>
        )}
        {user && (
          <Fragment>
            <hr />
            <PlaceDetailsImageForm
              placeId={place && place._id}
              uploadPlacePhoto={uploadPlacePhoto}
            />
          </Fragment>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  place: state.places.placeInfo,
  placeLoading: state.places.loading,
  comments: state.comments.comments,
  commentsLoading: state.comments.loading,
  photos: state.photos.photos,
  photosLoading: state.photos.loading
});

const mapDispatchToProps = dispatch => ({
  fetchPlaceInfo: placeId => dispatch(actions.fetchPlaceInfo(placeId)),
  fetchPlaceComments: placeId => dispatch(actions.fetchPlaceComments(placeId)),
  fetchPlacePhotos: placeId => dispatch(actions.fetchPlacePhotos(placeId)),
  createPlaceComment: (placeId, comment, ratings) =>
    dispatch(actions.createPlaceComment(placeId, comment, ratings)),
  deletePlaceComment: (commentId, placeId) =>
    dispatch(actions.deletePlaceComment(commentId, placeId)),
  uploadPlacePhoto: (placeId, images) => dispatch(actions.uploadPlacePhoto(placeId, images)),
  deletePlacePhoto: (photoId, placeId) => dispatch(actions.deletePlacePhoto(photoId, placeId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaceDetails);
