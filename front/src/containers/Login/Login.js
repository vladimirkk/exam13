import React, { Component } from "react";
import { Button, Form, FormGroup, Input } from "reactstrap";
import "./Login.css";
import { connect } from "react-redux";
import * as actions from "../../store/actions";

class Login extends Component {
  state = {
    username: "",
    password: ""
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    const onFormSubmit = event => {
      event.preventDefault();

      this.props.loginUser({ ...this.state });
    };

    return (
      <Form onSubmit={onFormSubmit} className="Login">
        <h2>Please sign in</h2>
        <FormGroup>
          <Input
            type="text"
            name="username"
            placeholder="Login"
            value={this.state.username}
            onChange={this.onChange}
          />
        </FormGroup>
        <FormGroup>
          <Input type="password" name="password" placeholder="Password" onChange={this.onChange} />
        </FormGroup>
        <Button size="lg">Sign in</Button>
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(actions.loginUser(userData))
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
