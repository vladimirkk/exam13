import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions";
import { Button, Col, Modal, ModalBody, ModalFooter, Row } from "reactstrap";
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import { apiURL } from "../../constants";
import Preloader from "../../components/UI/Preloader/Preloader";
import "./Places.css";

class Places extends Component {
  state = {
    confirmDeleteId: null
  };

  componentDidMount() {
    this.props.fetchPlaces();
  }

  openConfirm = placeId => this.setState({ confirmDeleteId: placeId });
  closeConfirm = () => this.setState({ confirmDeleteId: null });

  deletePlace = () => {
    this.props.deletePlace(this.state.confirmDeleteId);
    this.closeConfirm();
  };

  render() {
    if (this.props.loading)
      return (
        <Fragment>
          <h2>All places</h2>
          <Preloader />
        </Fragment>
      );

    const cards = this.props.places.map(place => {
      const confirmDelete = () => {
        this.openConfirm(place._id);
      };

      return (
        <Col key={place._id} sm={6} md={4} lg={3}>
          <PlaceCard
            id={place._id}
            title={place.title}
            image={`${apiURL}/uploads/${place.image}`}
            rating={place.rating}
            reviews={place.comments}
            photos={place.photos}
            user={this.props.user}
            deletePlace={confirmDelete}
          />
        </Col>
      );
    });

    return (
      <Fragment>
        <h2>All places</h2>
        <Row className="Places">
          {cards.length === 0 ? <Col tag="p">No places yet...</Col> : cards}
        </Row>
        {this.state.confirmDeleteId !== null && (
          <Modal isOpen={true} toggle={this.closeConfirm}>
            <ModalBody>Are you sure you want to delete this cafe?</ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.deletePlace} size="sm">
                Continue
              </Button>
              <Button color="secondary" onClick={this.closeConfirm} size="sm">
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  places: state.places.places,
  loading: state.places.loading,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  fetchPlaces: () => dispatch(actions.fetchPlaces()),
  deletePlace: placeId => dispatch(actions.deletePlace(placeId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Places);
