import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions";
import { Button, Col, Form, FormGroup, Input, Label } from "reactstrap";

const AddPlace = ({ addPlace }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [confirmation, setConfirmation] = useState(false);

  const onChangeTitle = event => setTitle(event.target.value);
  const onChangeDescription = event => setDescription(event.target.value);
  const onChangeImage = event => setImage(event.target.files[0]);
  const onChangeConfirmation = event => setConfirmation(event.target.checked);

  const onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    formData.append("title", title);
    formData.append("description", description);
    formData.append("image", image);
    formData.append("confirmation", JSON.stringify(confirmation));

    addPlace(formData);
  };

  return (
    <Fragment>
      <h2>Add new place</h2>
      <Form onSubmit={onSubmit}>
        <FormGroup row>
          <Label for="title" sm={3}>
            Title
          </Label>
          <Col sm={9}>
            <Input
              type="text"
              name="title"
              id="title"
              value={title}
              onChange={onChangeTitle}
              required
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="description" sm={3}>
            Description
          </Label>
          <Col sm={9}>
            <Input
              type="textarea"
              name="description"
              id="description"
              rows={5}
              value={description}
              onChange={onChangeDescription}
              required
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="image" sm={3}>
            Main photo
          </Label>
          <Col sm={9} style={{ display: "flex", alignItems: "center" }}>
            <Input
              type="file"
              name="image"
              id="image"
              accept="image/*"
              onChange={onChangeImage}
              required
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="confirmation" sm={3}>
            By submitting this form, you agree that the following information will be submitted to
            the public domain, and administrators of this site will have full control over the said
            information.
          </Label>
          <Col sm={{ size: 9 }}>
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  id="confirmation"
                  checked={confirmation}
                  onChange={onChangeConfirmation}
                />
                I understand
              </Label>
            </FormGroup>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col sm={{ size: 3 }}>
            <Button disabled={!confirmation} block>
              Submit
            </Button>
          </Col>
        </FormGroup>
      </Form>
    </Fragment>
  );
};

const mapDispatchToProps = dispatch => ({
  addPlace: placeData => dispatch(actions.addPlace(placeData))
});

export default connect(
  null,
  mapDispatchToProps
)(AddPlace);
