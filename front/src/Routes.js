import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Places from "./containers/Places/Places";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPlace from "./containers/AddPlace/AddPlace";
import PlaceDetails from "./containers/PlaceDetails/PlaceDetails";

const ProtectedRoute = ({ isAllowed, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/" />;
};

const Routes = ({ user }) => {
  return (
    <Switch>
      <Route path="/" exact component={Places} />
      <Route path="/places" exact component={Places} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
      <ProtectedRoute isAllowed={!!user} path="/places/add" exact component={AddPlace} />
      <Route path="/places/:id" exact component={PlaceDetails} />
      <Route render={() => <h2>Page not found...</h2>} />
    </Switch>
  );
};

export default Routes;
