import React, { Fragment } from "react";
import { Container } from "reactstrap";
import Header from "../../containers/Header/Header";
import "./Layout.css";

const Layout = props => (
  <Fragment>
    <Header />
    <Container tag="main" className="Layout-content">
      {props.children}
    </Container>
  </Fragment>
);

export default Layout;
