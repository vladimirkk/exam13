import React from "react";
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from "reactstrap";
import { NavLink } from "react-router-dom";
import Rating from "../UI/Rating/Rating";
import photoIcon from "../../assets/images/photo.svg";
import deleteIcon from "../../assets/images/delete.svg";
import "./PlaceCard.css";

const PlaceCard = ({ id, title, image, rating, reviews, photos, user, deletePlace }) => {
  const deleteHandler = event => {
    event.stopPropagation();
    deletePlace();
  };

  return (
    <Card className="PlaceCard">
      {user && user.role === "admin" && (
        <button type="button" onClick={deleteHandler}>
          <img src={deleteIcon} alt="" />
        </button>
      )}
      <NavLink to={`/places/${id}`}>
        <CardImg top width="auto" height={160} src={image} alt="Place image" />
      </NavLink>
      <CardBody>
        <CardTitle>
          <NavLink to={`/places/${id}`}>{title}</NavLink>
        </CardTitle>
        <CardSubtitle>
          <Rating value={rating} />
        </CardSubtitle>
        <CardText tag="div">
          <div>{`(${rating > 0 ? rating.toFixed(1) + ", " : ""}${reviews} Reviews)`}</div>
          <div className="PlaceCard-photos">
            <img src={photoIcon} height={20} alt="" />
            <span>{`${photos} photos`}</span>
          </div>
        </CardText>
      </CardBody>
    </Card>
  );
};

export default PlaceCard;
