import React from "react";
import ReactRating from "react-rating";
import "./Rating.css";

import emptyStar from "../../../assets/images/star-empty.png";
import fullStar from "../../../assets/images/star-full.png";

const Rating = ({ value, readonly = true, size = 20, fractions = 10, ...props }) => {
  return (
    <ReactRating
      {...props}
      className="Rating"
      emptySymbol={<img src={emptyStar} height={size} alt="" />}
      fullSymbol={<img src={fullStar} height={size} alt="" />}
      fractions={fractions}
      readonly={readonly}
      initialRating={value}
    />
  );
};

export default Rating;
