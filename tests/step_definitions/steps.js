const { I } = inject();
// Add in your custom step files

Given('Я авторизован', () => {
  I.amOnPage("login");
  I.fillField("Login", "user1");
  I.fillField("Password", "123");
  I.click("Sign in");
});

When('Я перехожу на страницу добавления заведений', () => {
  I.waitForText("Test User #1", 3);
  I.click(".HeaderUserDropdown");
  I.click("Add new place");
});

Then('Я вижу надпись {string}', text => {
  I.waitForText(text, 5);
});

Then('Я заполняю поле {string} значением {string}', (fieldName, value) => {
  I.fillField(fieldName, value);
});

Then('Я загружаю изображение в поле {string}', fieldName => {
  I.attachFile(fieldName, "data/image.jpg")
});

Then('Я ставлю галочку на поле {string}', fieldName => {
  I.checkOption(fieldName);
});

Then('Я жму {string}', button => {
  I.click(button);
});

When('Я перехожу на страницу заведения {string}', text => {
  I.waitForText(text);
  I.click(text);
});

Then('Я ставлю оценку {string} в поле {string}', (rating, fieldName) => {
  I.click(`${fieldName} > span:nth-child(${rating})`);
});