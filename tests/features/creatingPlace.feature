Feature: Добавление заведения
  Scenario: Успешное добавление заведения
    Given Я авторизован
    When Я перехожу на страницу добавления заведений
    Then Я вижу надпись "Add new place"
    And Я заполняю поле "Title" значением "New place"
    And Я заполняю поле "Description" значением "Test description"
    And Я загружаю изображение в поле "Main photo"
    And Я ставлю галочку на поле "I understand"
    And Я жму "Submit"
    Then Я вижу надпись "Place added successfully!"

