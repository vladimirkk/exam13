Feature: Добавление рецензии
  Scenario: Успешное добавление рецензии
    Given Я авторизован
    When Я перехожу на страницу заведения "Some cafe"
    Then Я вижу надпись "Add review"
    And Я заполняю поле "#reviewText" значением "Some test comment"
    And Я ставлю оценку "2" в поле "#qualityOfFood"
    And Я ставлю оценку "4" в поле "#serviceQuality"
    And Я ставлю оценку "3" в поле "#interior"
    And Я жму "Add"
    Then Я вижу надпись "Comment successfully added."
